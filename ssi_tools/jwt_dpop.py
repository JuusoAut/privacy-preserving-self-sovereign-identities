from jwcrypto.common import  base64url_decode, base64url_encode
from jwcrypto import jwk, jws, jwt
import json, datetime, base58, uuid, hashlib


"""Functions for creating JWTs and DPoP (demonstrate proof of possession) proofs"""

def create_dpop(private_key_file: str, http_method: str, issuer_url: str,
               jwt_str: str) -> str:
    """
    Creates DPoP over JWT
    
    :param str private_key_file: file containg private key used for signing in PEM format
    :param str http_method: HTTP method to be included in DPoP ("htm" claim)
    :param str issuer_url: Issuer's URL to be included in DPoP ("htu" claim)
    :param str jwt_str: JWT over which hash is calculated ("ath" claim)
    
    :return: DPoP as str
    :rtype: str
    """
    
    # open private key file
    with open(private_key_file, "rb") as private_key_fd:
        data = private_key_fd.read()
        key = jwk.JWK.from_pem(data)
        private_key_fd.close()

    # calculate hash over JWT        
    jwt_hash = base64url_encode(hashlib.sha256(jwt_str.encode('utf-8')).digest())
    
    jwk_key = key.export_public(as_dict=True)
    del jwk_key["kid"] # remove 'kid' since it's not included in DID
    dpop_header = {
        "typ": "dpop+jwt",
        "alg": "EdDSA",
        "jwk": jwk_key
    }
    
    dpop_claims = {
        "jti": str(uuid.uuid4()),
        "htm": http_method,
        "htu": issuer_url,
        "iat": int(datetime.datetime.now().timestamp()),
        "ath": jwt_hash
    }
    

    # sign DPoP
    dpop = jwt.JWT(header=dpop_header, claims=dpop_claims)
    dpop.make_signed_token(key)
    return dpop.serialize()


def create_jwt(private_key_file: str, subject_did: str, vc_template_file: str, 
               did_method: str, validity_time: int) -> str:
    """
    Creates JWT over Verifiable Credential for the subject
    
    :param str private_key_file: file containg private key used for signing in PEM format
    :param str subject_did: DID of the subject
    :param str vc_template_file: file of the Verifiable Credential template
    :param str did_method: DID method to use for the issuer (did:key or did:self)
    :param int validity_time: validity time of JWT in seconds
    
    :return: JWT as str
    :rtype: str
    """
    
    # open private key file
    with open(private_key_file, "rb") as private_key_fd:
        data = private_key_fd.read()
        key = jwk.JWK.from_pem(data)
        private_key_fd.close()
        

    # open credential template
    with open(vc_template_file, "r") as credential_file:
        credential = json.load(credential_file)
        credential_file.close()

        
    # generate issuer DID based on private key
    key_data = key.export(as_dict=True)
    
    if (did_method == "did:key"):
        issuer_did = "did:key:z" + \
                        base58.b58encode(b'\xed\x01' + base64url_decode(key_data['x'])).decode()
    else: # did:self
        issuer_did = "did:self:" + key_data['x']
        
    
    # add issuer DID, subject DID, issuance date, and expiration date to the credential
    credential['issuer'] = {}
    credential['issuer']['id'] = issuer_did
    
    credential['credentialSubject']['id'] = subject_did
    
    issuance_date = datetime.datetime.now(datetime.timezone.utc)
    issuance_date = issuance_date.replace(microsecond = 0) # remove microseconds
    credential['issuanceDate'] = issuance_date.isoformat()
    expiration_date = issuance_date + datetime.timedelta(0, validity_time)
    credential['expirationDate'] = expiration_date.isoformat()

    # create a signed JWT, first create JWT header and claims
    jwt_header = {
        "alg": "EdDSA",
        "typ": "JWT"
    }
    
    jwt_claims = {
        "iss": credential['issuer']['id'],
        "sub": credential['credentialSubject']['id'],
        "nbf": issuance_date.timestamp(),
        "exp": expiration_date.timestamp(),
        "jti": credential['id'],
        "vc": credential
    }
    
    # sign JWT
    jwt_vc = jwt.JWT(header=jwt_header, claims=jwt_claims)
    jwt_vc.make_signed_token(key)
    
    return jwt_vc.serialize()
