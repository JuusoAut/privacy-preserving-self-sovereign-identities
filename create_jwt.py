from jwcrypto import jwk, jws, jwt
import argparse

import ssi_tools.jwt_dpop as jwt_dpop


if __name__ == "__main__":
    """Creates JWT based on the Verifiable Credential"""
    
    parser = argparse.ArgumentParser()
    parser.add_argument('private_key_file', type=str,
                        help='PEM file containing private key for signing JWT')
    parser.add_argument('subject_did', type=str,
                        help='DID of credential subject')
    parser.add_argument('vc_template_file', type=str,
                        help='Verifiable Credential template file in JSON format')
    parser.add_argument('--did_method', type=str,
                        help='Method for issuer DID (did:self or did:key, default: did:self)')
    parser.add_argument('--validity_time', type=int,
                        help='Validity time of Verifiable Credential in seconds \
                        (default: 31536000 seconds (one year))')
    args = parser.parse_args()
    
    # check DID method
    if (args.did_method == None):
        args.did_method = "did:self" # default method
        
    if (args.did_method != "did:key") and (args.did_method != "did:self"):
        print("Error: Invalid did method, must be 'did:self' or 'did:key'")
        exit()

    # check validity time
    if (args.validity_time == None):
        args.validity_time = 31536000 # one year
        
    if (args.validity_time < 0):
        print("Error: Validity time must be positive:", args.validity_time)

    jwt_vc = jwt_dpop.create_jwt(args.private_key_file, args.subject_did, args.vc_template_file,
               args.did_method, args.validity_time)
    
    print(jwt_vc)

    # test verifying
    #print("\nVerifying...\n")
    #try:
    #    decoded_token = jwt.JWS()
    #    decoded_token.deserialize(jwt_vc)
        #key['x'] = key['x']+"2" # test for wrong key
    #    with open(args.private_key_file, "rb") as private_key_file:
    #        data = private_key_file.read()
    #        key = jwk.JWK.from_pem(data)
    #        private_key_file.close()
    #        decoded_token.verify(key)
    #    print("decoded JWT: ", decoded_token.payload.decode())
    #except Exception as e:
    #    print("Error:", str(e))



