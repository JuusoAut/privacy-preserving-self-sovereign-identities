#include "config.h"
#include "jwt.h"
#include "utils.h"

namespace jwt { 

std::vector<String> trustedDIDs {
    "did:self:U1SFfNLQWY6g1-n1RB_Qe_4FTMTght0woT-QOcdR9ow",
    "did:key:z6Mkk4YdpLxAxWkDULdBVifCjVDPh3WvhbkDL1W4miwoHEQb"
};

std::map<String, std::vector<String>> capabilityMap {
    {BASE_URL + String("/sensor1"), {"READ"}},
    {BASE_URL + String("/sensor2"), {"READ"}},
    {BASE_URL + String("/lamp1"), {"READ", "WRITE"}}
};


/**
 * @brief Verifies Authorization and DPoP headers of the request and provides access to the resource
 * @param request AsyncWebServerRequest
 */    
void verify_request(AsyncWebServerRequest *request) {
    
    if (!request->hasHeader("Authorization") || !request->hasHeader("dpop")) {
        request->send(401, "text/plain", "Error: No Authorization or dpop header present");
        return;
    }
    
    uint8_t subject_key[KEY_SIZE]; // subject's public key from JWT
    uint8_t jwt_hash[HASH_SIZE]; // hash over JWT
    
    unsigned long start, stop;

    start = micros();
    // verify JWT and DPoP
    if (!verify_jwt(request, subject_key, jwt_hash))
        return;
    
    if (!verify_dpop(request, subject_key, jwt_hash))
        return;
    stop = micros();
    Serial.printf("Time for JWT+DPoP verification: %u ms\n", (stop-start)/1000);
    
    request->send(200, "text/plain", request->url() + "\n");
}    


/**
 * @brief Verifies Verifiable Credential encoded as JWT, also extracts subject's public key and hash
 * @details Checks Authorization type, "iss" claim, Access Control List, and signature.
 * Extracts subject's public key and calculates hash over whole JWT
 * @param request AsyncWebServerRequest
 * @param subject_key subject's public key present in the JWT will be copied here
 * @param hash_buffer hash over JWT will be stored here
 * @return status of verification (true/false)
 */
bool verify_jwt(AsyncWebServerRequest *request, uint8_t *subject_key, uint8_t *jwt_hash) {
    AsyncWebHeader *header = request->getHeader("Authorization");
    
    String JWT = header->value();
    // 1. Check Authorization type
    if (!JWT.substring(0, 5).equals("DPoP ")) { 
        request->send(401, "text/plain", "Error: Invalid authorization type\n");
        return false;
    }
    
    JWT.remove(0, 5); // remove "DPoP " from the beginning
    
    String JWT_payload = JWT.substring(JWT.indexOf('.') + 1, JWT.lastIndexOf('.'));

    // 2. Decode and deserialize JSON for JWT payload
    size_t outputLength;
    unsigned char *decoded = utils::base64url_decode(JWT_payload, &outputLength);

    StaticJsonDocument<5000> jwt_document;
    DeserializationError err = deserializeJson(jwt_document, decoded);
    if (err) { // Check for errors in JSON parsing
        request->send(401, "text/plain", "Error: Failed to parse JWT JSON\n");
        
        if (decoded != NULL)
            free(decoded);
        return false;
    }

    // 3 check if "iss" claim of JWT is trusted
    String iss = jwt_document["iss"];
    if (std::find(trustedDIDs.begin(), trustedDIDs.end(), iss) == trustedDIDs.end()) {
        request->send(401, "text/plain", "Error: Issuer of JWT-VC not trusted ('iss' claim)\n");
        free(decoded);
        return false;
    }

    // TODO: check credential validity time (need to know accurate time for this)

    // 4. Verify capabilities of credential
    // check that the requested resource exists in the capabilityMap
    std::vector<String> capabilities = capabilityMap[BASE_URL + request->url()];
    if (capabilities.empty()) {
        request->send(401, "text/plain", "Error: No capabilities defined for the resource on the device\n");
        free(decoded);
        return false;
    }
    
    // check that the right capability is present in the credential
    JsonArray array = jwt_document["vc"]["credentialSubject"]["capabilities"][BASE_URL + request->url()];
    bool match = false;
    for (JsonVariant v : array) {
        // READ capability is for HTTP_GET
        // WRITE capability is for HTTP_PUT, HTTP_POST, or HTTP_DELETE
        if (((request->method() == HTTP_GET) && v.as<String>().equals("READ")) ||
                ((request->method() == HTTP_PUT) || (request->method() == HTTP_POST) ||
                (request->method() == HTTP_DELETE)) && (v.as<String>().equals("WRITE"))) {
            if (std::find(capabilities.begin(), capabilities.end(), v.as<String>()) != capabilities.end()) {
                match = true;
                break;
            }
        }
    }
    
    if (!match) {
        request->send(401, "text/plain", "Error: No sufficient capabilities present in credential\n");
        free(decoded);
        return false;
    }
    
    
    // 5. Verify JWT signature
    // parse issuer's public key from DID
    uint8_t *issuer_key = utils::did_to_public_key(iss, &outputLength);
    if (issuer_key == NULL) {
        request->send(401, "text/plain", "Error: Unknown DID method\n");
        free(decoded);
        return false;
    }
    
    if (!jwt::verify_jwt_signature(JWT, issuer_key)) {
        request->send(401, "text/plain", "Error: Invalid JWT-VC signature\n");
        free(decoded);
        free(issuer_key);
        return false;
    }
    free(issuer_key);
    
    // 6. Extract subject's public key
    String sub = jwt_document["sub"];
    uint8_t *tmp_subject_key = utils::did_to_public_key(sub, &outputLength);
    if (tmp_subject_key == NULL) {
        request->send(401, "text/plain", "Error: Unknown DID method\n");
        free(decoded);
        return false;
    }
    // did_to_public_key returns a new pointer, therefore we need to copy its content
    memcpy(subject_key, tmp_subject_key, KEY_SIZE);
    free(tmp_subject_key);
    free(decoded); // this should be done after JSON document isn't used anymore
    
    // 7. Calculate hash over JWT
    SHA256 sha256;
    Hash *hash = &sha256;
    hash->reset();
    hash->update(JWT.c_str(), JWT.length());
    hash->finalize(jwt_hash, HASH_SIZE);

    return true;
}


/**
 * @brief Verifies DPoP proof
 * @details Checks that subject's key matches with key from JWT, 'ath' claim matches with 
 * hash over JWT, and DPoP signature
 * @param request AsyncWebServerRequest
 * @param subject_key subject's public key from JWT
 * @param hash_buffer hash over JWT
 * @return status of verification (true/false)
 */ 
bool verify_dpop(AsyncWebServerRequest *request, uint8_t *subject_key, uint8_t *jwt_hash) {
    
    AsyncWebHeader *header = request->getHeader("dpop");

    // 1. Decode and deserialize JSON for DPoP header
    String DPoP_header = header->value().substring(0, header->value().indexOf('.'));
    size_t outputLength;
    unsigned char *decoded = utils::base64url_decode(DPoP_header, &outputLength);

    StaticJsonDocument<256> DPoP_header_document;
    DeserializationError err = deserializeJson(DPoP_header_document, decoded);
    if (err) { // Check for errors in JSON parsing
        request->send(401, "text/plain", "Error: Failed to parse DPoP header JSON\n");
        return false;
    }

    // 2. Check that subject key in DPoP header matches with the key in JWT-VC
    // get and decode public key
    String jwk_x = DPoP_header_document["jwk"]["x"];
    free(decoded);
    decoded = utils::base64url_decode(jwk_x, &outputLength);

    if (memcmp(subject_key, decoded, outputLength) != 0) { // check that keys match
        request->send(401, "text/plain", 
                      "Error: JWK key in DPoP header doesn't match with 'iss' claim of JWT\n");
        free(decoded);
        return false;
    }
    free(decoded);
    
    // 3. Decode and deserialize JSON for DPoP payload
    String DPoP_payload = header->value().substring(header->value().indexOf('.') + 1, 
                                                    header->value().lastIndexOf('.'));

    decoded = utils::base64url_decode(DPoP_payload, &outputLength);
    StaticJsonDocument<256> DPoP_payload_document;
    err = deserializeJson(DPoP_payload_document, decoded);
    if (err) { // Check for errors in JSON parsing
        request->send(401, "text/plain", "Error: Failed to parse DPoP header JSON\n");
        free(decoded);
        return false;
    }
    
    // TODO: check 'iat' claim (timestamp)
    // TODO: check the 'htu' claim (URL)
    
    // 4. Verify that 'ath' claim matches with the hash of whole JWT
    String ath = DPoP_payload_document["ath"];
    free(decoded);
    decoded = utils::base64url_decode(ath, &outputLength); // 'ath' claim is also base64url encoded

    if (memcmp(jwt_hash, decoded, outputLength) != 0) { // check that hash matches with decoded ath
        request->send(401, "text/plain", "Error: DPoP 'ath' claim doesn't match the hash of JWT\n");
        free(decoded);
        return false;
    }
    free(decoded);
    
    // 5. Verify DPoP signature (DPoP is also a JWT token)
    if (!jwt::verify_jwt_signature(header->value(), subject_key)) {
        request->send(401, "text/plain", "Error: Invalid DPoP signature\n");
        return false;
    }

    return true;
}


/**
 * @brief Verifies signature of JWT 
 * @param JWT JWT as String
 * @param public_key key used for verification
 * @return result of verification (true/false)
 */
bool verify_jwt_signature(String JWT, uint8_t public_key[]) {
    size_t outputLength;

    String message = JWT.substring(0, JWT.lastIndexOf('.')); // signed message: header.payload
    uint8_t *signature = utils::base64url_decode(JWT.substring(JWT.lastIndexOf('.') + 1), &outputLength);
    
    bool verified = Ed25519::verify(signature, public_key, message.c_str(), message.length());
    free(signature);
    
    return verified;
}

} // namespace jwt
