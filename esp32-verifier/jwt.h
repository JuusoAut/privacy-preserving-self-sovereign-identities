#ifndef JWT_H
#define JWT_H

#include <Arduino.h>
#include <map>

#include <SHA256.h>
#include <Ed25519.h>

#include "ArduinoJson.h"
#include "ESPAsyncWebServer.h"

//#define KEY_SIZE 32 // size of Ed25519 key in bytes
//#define HASH_SIZE 32 // size of SHA256 hash in bytes

namespace jwt { 
    
void verify_request(AsyncWebServerRequest *request);

bool verify_jwt(AsyncWebServerRequest *request, uint8_t *subject_key, uint8_t *jwt_hash);
bool verify_dpop(AsyncWebServerRequest *request, uint8_t *subject_key, uint8_t *jwt_hash);

bool verify_jwt_signature(String JWT, uint8_t public_key[]);

}

#endif // JWT_H
