# Resource server for ESP32 supporting SSI-based JWT and DPoP authentication

## Description

The repository contains a code that verifies Verifiable Credentials (encoded as JWT) and DPoP proofs on a ESP32 device. See the [main documentation](../) for more information about the credentials and proofs used.

The ESP32 device performs a full verification of JWT and DPoP, except for the validity time verification since by default the device lacks a knowledge of the current time. If the verification is succesful, the client gains a read or write access to the resource.


## Usage

### Prerequisites

ESP32 device, the code was tested on the Espressif ESP32 WROOM-32, other ESP32-based devices should also work.

**[Arduino IDE](https://www.arduino.cc/en/software)** with the support for the ESP32 board version 1.06 (https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html) and the following libraries:
- Crypto (available through Arduino Library Manager)
- ArduinoJson (available through Arduino Library Manager)
- Async TCP: https://github.com/me-no-dev/AsyncTCP/archive/master.zip
- ESPAsyncWebServer: https://github.com/me-no-dev/ESPAsyncWebServer/archive/master.zip

Instruction how to install libraries to Arduino IDE are available here: https://docs.arduino.cc/software/ide-v1/tutorials/installing-libraries .

### Configuration

[config.h](config.h) contains the SSID, password, and the IP address settings of the device's WiFi access point. It also contains the `BASE_URL`string that denotes the base URL (without the trailing slash) used in the "capabilities" key of the Verifiable Credential. For example, is the `BASE_URL` is `http://some.com` and the resource endpoint is `/sensor1`, then the credential should have capabitilies defined for the URL: `http://some.com/sensor1`.

DIDs of trusted issuers are defined in `trustedDIDs` vector within the [jwt.cpp](jwt.cpp) file. `capabilityMap` within the same file defines which capabilities the credential should have in order to access specific endpoints.

In order to add a new resource endpoint, the following should be done:
1. In the setup() function of the [esp32.ino](esp32.ino), call server.on(), e.g. `server.on("/sensor1", HTTP_GET, jwt::verify_request);`.
2. Modify `capabilityMap` to define required capabilities to access the endpoint ([jwt.cpp](jwt.cpp)).
3. Add a custom code to handle the resource endpoint (e.g. read and return a sensor value) at the end of the `verify_request()` function ([jwt.cpp](jwt.cpp)).


### Installation

Open `esp32.ino` file in Arduino IDE and upload it to the device.


### Execution

Upon startup, the device will print its IP address (default: `192.168.2.1`) to serial console. Then connect to the access  point configured previously (default SSID/password is: test/test1234) and access the device resources providing the necessary JWT credential and DPoP proof.

The `ssi-example.sh` script can be used to as described in the [main documentation](../#example):
```bash
bash ssi-example.sh owner.pem client.pem credentials/example-vc.json http://192.168.2.1/sensor1
```

## Open Issues and Limitations

- 2.x versions of the Arduino-ESP32 support libraries conflict with the Crypto library, therefore version 1.06 should be used.

- Only the Ed25519 EdDSA signature scheme is supported for verification of cryptographic signatures.

- The code does not verify timestamps of the credentials or the DPoP proof, since by default the device is not aware of the current time.

## Future Work

- Support for two-way authentication (device also provides its credentials).

## Release Notes

## Contact Information

Contact: Dmitrij Lagutin (dmitrij.lagutin _at_ aalto.fi)
