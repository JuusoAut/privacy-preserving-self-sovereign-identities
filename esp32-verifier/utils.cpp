#include "config.h"
#include "utils.h"


namespace utils { 

// array for base58 decoding
const int8_t base58_map[] = {
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     1,  2,  3,  4,  5,  6,  7,  8, -1, -1, // numbers 2-9
    -1, -1, -1, -1, -1,  9, 10, 11, 12, 13, // A-E
    14, 15, 16,  0, 17, 18, 19, 20, 21, -1, // E-H and J-N
    22, 23, 24, 25, 26, 27, 28, 29, 30, 31, // P-Y
    32, -1, -1, -1, -1, -1, -1, 33, 34, 35, // Z and a-c
    36, 37, 38, 39, 40, 41, 42, 43,  0, 44, // d-k and m
    45, 46, 47, 48, 49, 50, 51, 52, 53, 54, // n-w
    55, 56, 57};                            // x-z


/**
 * @brief Decodes base58-encoded String and stores decoded value to destination buffer
 * @param b58_source Source String to decode
 * @param destination destination buffer (memory must be allocated in advance)
 * @param dest_size size of destination buffer
 * @return status of decoding (true/false)
 */ 
bool base58_decode(const String b58_source, uint8_t *destination, int dest_size) {
    int carry = 0;
    uint8_t input;
    unsigned int tmp;
    
    /*
    From RFC: For each input byte in the array of input bytes:
    1. Set 'carry' to the byte value associated with the input byte character. 
        If a mapping does not exist, return an error code.
    2. While 'carry' does not equal zero and there are input bytes remaining:
        2.1 Multiply the input byte value by 58 and add it to 'carry'.
        2.2 Set the output byte value to 'carry' modulus 256.
        2.3 Set 'carry' to the value of 'carry' divided by 256.
    3. Set the corresponding byte value in 'raw_bytes' to the value of 'carry' modulus 58.
    4. Set 'carry' to the value of 'carry' divided by 58.
    */
    
    bzero(destination, dest_size);
    for (int i = 0; i < b58_source.length(); i++) {
        
        input = b58_source.charAt(i);
        if ((input < 49) || (input > 123)) // invalid input value
            return false;
        
        carry = base58_map[input];
        if (carry == -1) // invalid input value
            return false;
        
        for(int j = dest_size - 1; j >= 0 ; j--) {
            tmp = destination[j] * 58 + carry;
            carry = (tmp & (~0xff)) >> 8;
            destination[j] = tmp & (0xff);
        }
    }
    
    return true;
}


/**
 * @brief Decodes base64url encoded string
 * @param payload string to decode
 * @param outputLength length of the decoded string
 * @return decoded string
 */
unsigned char *base64url_decode(const String payload, size_t *outputLength) {
    String str = payload;
    
    // convert base64url encoding to base64 encoding
    str.replace('-','+');
    str.replace('_','/');
    
    // base64_decode() requires padding even thought it's optional...
    while ((str.length() % 4) > 0) {
      str += "=";
    }
    
    return base64_decode((const unsigned char *)str.c_str(), str.length(), outputLength);
}


/**
 * @brief Converts DID to public key, supports did:self and did:key methods
 * @param did DID to convert
 * @param outputLength length of decoded public key
 * @return public key as uint8_t pointer
 */
uint8_t *did_to_public_key(const String did, size_t *outputLength) {
    
    uint8_t *public_key, *tmp;

    if (did.substring(0, 8).equals("did:self")) {
        public_key = utils::base64url_decode(did.substring(9), outputLength);
    
    } else if (did.substring(0, 7).equals("did:key")) {
        // for did:key there are two bytes in the beginning that denote the algorithm used
        // TODO: add support for other did:key algorithms beside Ed25519
        tmp = (uint8_t *)malloc(KEY_SIZE + 2);
        if (utils::base58_decode(did.substring(9), tmp, KEY_SIZE + 2)) {
            public_key = (uint8_t *)malloc(KEY_SIZE);
            memcpy(public_key, tmp + 2, KEY_SIZE);
            free(tmp);
        } else {
            free(tmp);
            return NULL;
        }
      
    } else { // unknown DID method
        return NULL;
    }
    return public_key;
}



} // namespace utils
