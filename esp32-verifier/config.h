#ifndef CONFIG_H
#define CONFIG_H

#define KEY_SIZE 32 // size of Ed25519 key in bytes
#define HASH_SIZE 32 // size of SHA256 hash in bytes

// WiFi SSID and password
#define WIFI_SSID "test"
#define WIFI_PASSWORD "test1234"

// WiFi IP address, gateway, and subnet
#define IP 192,168,2,1
#define GATEWAY 192,168,2,1
#define SUBNET 255,255,255,0

// Base URL of capabilities
#define BASE_URL "https://devices.iot-ngin.eu"

#endif // CONFIG_H
