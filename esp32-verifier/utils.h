#ifndef UTILS_H
#define UTILS_H

#include <Arduino.h>

extern "C" {
#include "crypto/base64.h"
}



namespace utils { 

bool base58_decode(const String b58_source, uint8_t *destination, int dest_size);    
    
unsigned char *base64url_decode(const String payload, size_t *outputLength);

uint8_t *did_to_public_key(const String did, size_t *outputLength);

}

#endif // UTILS_H
