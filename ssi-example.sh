#!/bin/bash

if [ $# -lt 4 ]
  then
    echo "Usage: example.sh owner_private_key_file user_private_key_file credential_file resource_url [-did:key]"
    exit
fi

# check for optional -did:key argument
EXTRA_ARGS=""
if [ ! -z "$5" ]
  then
    if [ "$5" == "-did:key" ]
    then
        EXTRA_ARGS="--did_method did:key"
    fi
fi

#1. get user's DID
DID=`python3 key_tools.py show_did $2 $EXTRA_ARGS`

#2. generate JWT-encoded VC for the user DID using owner keyfile
JWT=`python3 create_jwt.py $1 $DID $3 $EXTRA_ARGS`

#3. generate DPoP using user keyfile
DPOP=`python3 create_dpop.py $2 GET $4 $JWT`

#4. send a JWT + DPoP request to the server
curl -H "Authorization: DPoP $JWT" -H "dpop: $DPOP" -H "Accept: application/json" $4

