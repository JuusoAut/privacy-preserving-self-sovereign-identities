
import argparse

import ssi_tools.jwt_dpop as jwt_dpop

if __name__ == "__main__":
    """Creates DPoP proof based on JWT"""
    
    parser = argparse.ArgumentParser()
    parser.add_argument('private_key_file', type=str,
                        help='PEM file containing private key for signing DPoP')
    parser.add_argument('http_method', type=str,
                        help='HTTP method to be used with a proof (GET/POST/...)')
    parser.add_argument('issuer_url', type=str,
                        help='Issuer URL (htu field of DPoP)')
    parser.add_argument('JWT', type=str, help='JWT over which DPoP proof is generated')
    args = parser.parse_args()
    
    dpop = jwt_dpop.create_dpop(args.private_key_file, args.http_method, args.issuer_url, args.JWT)
    print(dpop)
    
